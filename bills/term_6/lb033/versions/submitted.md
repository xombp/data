# 1 Definition

1. In the below bill, "applicant" is defined as any persons not born in the United Kingdom or her overseas territories and not the child of two parents who are already citizens of the United Kingdom.

## 2 Naturalization
1. All applicants will need to go through the entirety of the Naturalization process (as defined in the below sections and by the British Nationality Act 1981) in order to gain citizenship, and must possess Indefinite Leave to Remain status for 7 years before applying.
2. Those seeking Indefinite Leave to Remain status must remain, at all times, within the United Kingdom's borders for a period of 16 months, and must not leave the United Kingdom for more then a month total every year afterward to maintain Indefinite Leave to Remain status.
3. Having "Good character", as referred to in the British Nationality Act 1981 and all past and future immigration laws, shall be defined as:
    1. Lacking a criminal record in any previous countries of residence,
    2. Possessing letters from at least 5 colleagues in all previous countries of residence stating the applicant has a good moral compass and understanding of justice,
    3. Possessing a credit score of 750 or greater.
4. Over security and forgery concerns, all required forms, paperwork, and fees for citizenship application must be filled out and turned in at the Home Office's 2 Marsham Street headquarters.
5. All migrants who have previously gained citizenship before this law's passing who do not fulfil these requirements will have their citizenship suspended until the requirements are met.
 
## 3 Short title, commencement and extent
1. This Act may be cited as the Citizenship Reform Act 2021.
2. This Act comes into force on the passing of this Act.
3. This Act extends to the United Kingdom of Great Britian and Northern Ireland and her overseas territories.
