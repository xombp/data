# News Liberalization Bill
A bill to reorganize the British news and media, making it just and fair for all Britons

Be it enacted by the Queen's most Excellent Majesty, by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:—

## 1 Organization of Government Offices
1. Recognizing that the Office of Communications has harmed British reporting and media in general, subject to the below sections, the Office will be privatized.
2. The Communications Acts of 2002 and 2003, and any other laws dealing with the Office of Communication's roles and responsibilities, are repelled by this law's passing.
3. All assets of the Office of Communications, including its Riverside House headquarters, employee contracts, and any other items owned by the Office of Communications are to be auctioned off to no less then 4 British media corporations.
4. All laws regulating sponsorships in media, particularly in news or children's media, are to be repealed.

## 2 Short title, commencement and extent
1. This Act may be cited as the News Liberalization Act 2021.
2. This Act comes into force on the passing of this Act.
3. This Act extends to the United Kingdom of Great Britian and Northern Ireland and her overseas territories.