## Section 1
1. This act will seek to provide national representation for all British subjects.
2. This act will place the Crown Dependencies and Overseas Territories under existing constituencies or in a special “Overseas Constituency” depending on the circumstance of the election and the will of the Speaker.
3. The Crown Dependencies will be placed into constituencies as follows: Jersey and Guernsey will be placed in the Southern England Constituency, the Isle of Man will be placed in the Northern England Constituency.
4. As the most cosmopolitan region, the British Overseas Territories will be placed in the Constituency of London.

## Section 2
1. The Dependencies and Overseas Territories shall retain their current level of devolution outside of receiving national representation.

## Section 3
1. This act will come into effect upon passage.
2. This act can be referred to as “The National Representation Act of 2021” or “The National Representation Act”
3. This bill extends to the United Kingdom, Crown Dependencies, and British Overseas Territories

