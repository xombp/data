# Rail Safety Bill 2022
A bill to improve safety on Britain's rail network

Be it enacted by the Queen's most Excellent Majesty, by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:�

## 1 Crew operations

1. Any freight train that is 775 meters or longer in length, and which is traveling at least 400 kilometers without stopping, must employ at least two crew members for the full length of its journey.
2. Any passenger train that is 240 meters or longer in length must employ at least two crew members for the full length of its journey.

## 2 Train Protection & Warning System

1. The Chair of the Office of Rail and Road shall form a committee to examine the future of the Train Protection & Warning System (TPWS).
   1. The committee will be tasked with examining:
      1. The estimated lifespan of current TPWS equipment
      2. High-risk locations without TPWS+ where the TPWS+ system can be feasibly introduced
      3. The estimated cost for any necessary upgrades or improvements to the TPWS equipment
   2. Upon the conclusion of its work, the committee shall present its findings in a report to the Chair of the Office of Rail and Road.

## 3 Heat-resistant steel

1. Steel used in the manufacturing of railway tracks for use within England must be stress-tested at temperatures of 30 degrees Celsius or higher.
   1. This requirement will rise by 2 degrees Celsius every year following the implementation of this legislation.
      1. The requirement will cease rising upon reaching 40 degrees Celsius.

## 4 Enforcement

1. The Office of Rail and Road shall be empowered to enforce the provisions of this act.
   1. The Office of Rail and Road shall have the authority to set appropriate fines and penalties for violations of this act.

## 5 Short title, commencement and extent

1. This Act may be cited as the Rail Safety Act 2022.
2. This Act comes into force six months after royal assent.
3. This Act extends to England.