# Energy (Emergency Support) Bill
*A Bill to respond to the ongoing Energy Crisis by nationalising the energy industry, providing support to the British Public, and buying energy for a reasonable rate.*

## 1 Definitions

1.	In this bill “Energy Suppliers” refers to companies that are responsible for selling Electricity or Natural Gas to consumers
2.	In this bill “Electricity Producers” Refers to companies that are responsible for producing electricity through Electric Power Stations
3.	In this bill “Power Stations” Refers to plants that produce electricity, whether through Nuclear Fission, the Combustion of Fossil Fuels, or Renewable Resources
4.	In this bill “Gas, Oil and other Energy distribution networks and storage depots” refers to the infrastructure required for the transmission or transportation or storage of Electricity, Oil or Natural Gas, including Power Lines and Gas and Oil pipelines
5.	In this bill “North Sea Gas and Oil” refers to Oil, Gas and other fossil fuels extracted from the British Exclusive Economic Zone in the North Sea and Atlantic

## 2 The Nationalisation of Energy

1.	The Secretary of State for Business and Industrial Strategy is authorised to begin the compulsory purchase at market rates of:
	1.	Energy Suppliers
	2.	The National Grid
	3.	Electricity Producers and Power Stations
	4.	Gas, Oil and other Energy distribution networks and storage depots
2.	The nationalised energy assets will be owned by a new publicly owned company named “British Energy”
3.	51% of the Shares of British Energy will be owned by the Department for Business, Energy, and Industrial Strategy
4.	25% of the Shares of British Energy will be owned by the employees of British Energy for the duration of their employment
5.	24% of the Shares of British Energy will be publicly traded
6.	British Energy will be under the review and regulation of Ofgem
7.	The Secretary of State is empowered to create a further framework for the creation of British Energy

## 3 North Sea Gas and Oil Producers

1.	North Sea Gas and Oil Producers cannot export energy without an export licence from the Department for Business, Energy, and Industrial Strategy
2.	Before a licence may be issued the shipment of oil or natural gas must be offered to British Energy for purchase at no more than a 20% profit margin
3.	Ofgem is empowered to investigate and enforce these rules
4.	Energy companies found to have exported oil or natural gas without an export licence may be subject to fines not totalling more than 30% of that companies annual income or having its drilling permits revoked and north sea infrastructure nationalised
5.	Energy companies found to have charged British Energy a price that gives a profit margin of greater than 20% may be subject to fines not totalling more than 20% of that companies annual income plus a refund of the excess profits.
6.	This section will expire six months after the passing of this act, unless the Secretary of State renews it for a period of time not greater than 12 months

## 4 Energy Support

1.	The Government will provide an increase of £50bn to the budget of the Winter Fuel Payment
2.	The Secretary of State will be allowed to extend the increase by up to an extra £50bn

## 5 Short Title, Commencement and Extent
1.	This Bill may be cited as the Energy (Emergency Support) Bill 2022
2.	This Act comes into force on the day on which the Act is passed
3.	This Act extends to the United Kingdom