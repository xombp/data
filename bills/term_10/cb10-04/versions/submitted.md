## 1 Referendums

1. Constituency MPs in Scotland, Wales, or Northern Ireland may call a referendum for issues concerning their respective constituencies at any time.
2. Approved referendums will be treated identically to legislation passed through Parliament, and will be subject to the limitations of any such legislation.
3. The voting date for any referendums called through this act will occur no later than 30 days after the calling of the referendum.

## 2 Short title, commencement, and extent

1. This Act may be cited as the Devolved Referendum Powers Act 2022.
2. This Act extends to Scotland, Wales, and Northern Ireland.
3. This Act comes into force upon royal assent.
