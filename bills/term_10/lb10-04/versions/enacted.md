## 1 Definition

1. A **payment processor** is any company, person, or organisation which enables financial transactions, including (but not limited to) issuers of credit cards and debit cards and operators of online payment systems.

## 2 Suspensions

1. A payment processor commits an offence if it denies services to a person or organisation for reasons related to that person or organisation’s lawful exercise of free speech or free expression.
2. A company, person, or organisation committing an offence under this section is liable on summary conviction to a fine not exceeding the statutory maximum, and may be made to pay damages to the affected person or organisation.
3. This bill shall not cover content that is deemed by the payment processor or by the relevant government authority to be hateful, defamatory, or which promotes misinformation.
4. If a producer of content believes they are subjected to unfair discrimination based on their exercise of free speech, they shall be allowed to appeal the payment processor’s decision to the Commission for Free Expression.
    1. The Department for Digital, Culture, Media and Sport and His Majesty’s Treasury will be jointly tasked with forming the Commission for Free Expression, which shall judge and arbitrate disputes and appeals relating to this legislation. The commission shall be composed of an equal number of members appointed by each department. The Secretary of State of each respective department may issue further regulations and statutory instruments as deemed necessary.

## 3 Short title, commencement, and extent

1. This Act may be cited as the Payment Processors and Speech Act 2023.
2. This Act extends to the entire United Kingdom of Great Britain and Northern Ireland.
3. This Act comes into force upon royal assent.
