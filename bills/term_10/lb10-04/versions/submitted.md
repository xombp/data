## 1 Definition

1. A **payment processor** is any company, person, or organisation which enables financial transactions, including (but not limited to) issuers of credit cards and debit cards and operators of online payment systems.

## 2 Suspensions

1. A payment processor commits an offence if it denies services to a person or organisation for reasons related to that person or organisation’s lawful exercise of free speech or free expression.
2. A company, person, or organisation committing an offence under this section is liable on summary conviction to a fine not exceeding the statutory maximum, and may be made to pay damages to the affected person or organisation.

## 3 Short title, commencement, and extent

1. This Act may be cited as the Payment Processors and Speech Act 2022.
2. This Act extends to the entire United Kingdom of Great Britain and Northern Ireland.
3. This Act comes into force upon royal assent.
