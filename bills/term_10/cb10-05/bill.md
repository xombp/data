
# Finance Bill

*A Bill to grant certain duties, to alter other duties, and to amend the law relating to the national debt and the public revenue, and to make further provision in connection with finance.*

## 1. Income tax charge, rates etc

1. Income tax is charged for the tax year 2021-22.
2. For the tax year 2021-22 the main rates of income tax are as follows—
	1. the basic rate is 20%,
	2. the higher rate is 45%, and
	3. the additional rate is 60%.

## 2. Corporation tax charge and rates

1. Corporation tax is charged for the financial year 2022
2. The main rate of corporation tax—
	1. is 33% for the financial year 2022

## 3. Dividend and buyback charge for tax year 2022-23

1. The dividend and buyback tax is charged for the financial year 2022
2. The main rate of the dividend and buyback tax-
	1. is 1% for the financial year 2022

## 4. Non-domiciled foreign income charge for tax year 2022-23

1. The non-domiciled foreign income tax is charged for the financial year 2022
2. For the tax year 2022-23 the main rates of non-domiciled foreign income tax are as follows—
	1. the basic rate is 20%,
	2. the higher rate is 45%, and
	3. the additional rate is 60%.

## 5. Private school fee charge for tax year 2022-23

1. The private school fee tax is charged for the financial year 2022
2. The main rate of the private school fee tax for tax year 2022-23
	1. is 20% for the financial year 2022

## 6. Windfall land value charge for tax year 2022-23

1. The windfall land value tax is charged for the financial year 2022
2. The main rate of the windfall land value tax for the financial year 2022
	1. is 2% for the financial year 2022

## 7. Frequent flyer levy for tax year 2022-23

1. The frequent flyer lever is charged for the financial year 2022
2. For the tax year 2022-23 the main rates of the frequent flyer levy are as follows
	1. the first flight shall be levy free
	2. the second flight shall see a levy of £25, and
	3. each subsequent flight shall see the levy increase by £10

## 8 Short title, commencement and extent

1. This Act may be cited as the Finance Act 2022.
2. This Act comes into force on the day on which the Act is passed.
3. This Act extends to the United Kingdom.