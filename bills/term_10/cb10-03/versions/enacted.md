## 1 GCSE and A Level Reform
1. The Secretary of State for Education is required:
   1. to change the course structure of the General Certificate of Secondary Education to a modular system.
   2. to change the course structure of the General Certificate of Education Advanced Level to a modular system.
2. The modular system must include:
   1. Multiple examinations taken across the period of the two year course
   2. A coursework component to be included in the second year


## 2 Apprenticeship Levy Increase
1. The Apprenticeship Levy is increased to 0.75%
2. The increase in the levy must be used to fund an expansion in the apprenticeship scheme


## 3 Academy Reform
1. The Secretary of State is required to stop the creation of new Academy Trusts
2. Any Academy Trust commits an offence when:
   1. It purchases goods or services from a company with links to the academy trust
   2. It purchases goods or services from a company with links to members of the Academy Trust
   3. It purchases goods or services from a company with interests from any senior member of staff at the academy trust
3. Any Academy Trust found guilty of committing said offence is to be issued a fine decided upon by the Charities Commission or have their status as charities revoked and their academies brought under the ownership of the Department for Education


## 4 Reducing Permanent Exclusions and Supporting SEN Students
1. In this section ‘SEN Student’ refers to Students with Special Educational Needs
2. In this Section ‘Local Authorities’ refers to Unitary Authorities, District Councils and London Boroughs
3. The Secretary of State for Education is required to produce a plan for the creation of additional schools specialising in the education of SEN Students
4. Schools must undergo mandatory mental health assessments for any student who is at risk of exclusion
5. Schools are required to create a plan for how to address persistent bad behaviour and deal with the root causes before it leads to exclusion
6. Local Authorities are required to monitor exclusion statistics from their area
7. Local Authorities are required to take action if exclusions in their area are disproportionately affecting areas of the community with protected characteristics as defined under the Equality Act 2010


# 5 Short title, extent, commencement
1. This act may be cited as the Education Reform Act 2023
2. This act extends to England and Wales.
3. This act comes into force upon royal assent
