## 1 Commission to advise on persons nominated for life peerages
1. There is to be a House of Lords Appointments Commission (“the Commission”) to advise the Prime Minister on recommendations to the Crown for the creation of peerages under the Life Peerages Act 1958.

## 2 Prime Minister to refer recommendations to the Commission
1. Before recommending any person to the Crown for a peerage, the Prime Minister must refer the name of that person to the Commission.
2. The Prime Minister may not recommend a person to the Crown for a peerage until such time as the Commission has advised the Prime Minister as to whether the person meets the criteria for the conferment of a peerage specified in section 7.
3. The Prime Minister may not recommend a person to the Crown for a peerage if the Commission has advised that the person does not meet the criteria for the conferment.

## 3 Principles to be followed in making recommendations
1. In determining whether to recommend to the Crown the creation of peerages, the Prime Minister must have regard to the following principles—
    1. not less than 20 per cent of the membership of the House of Lords shall consist of members who are independent of any registered political party,
    2. no one party may have an absolute majority of members in the House of Lords, and
    3. the membership of the House of Lords must be no larger than that of the House of Commons.
2. For the purpose of determining the membership specified in subsection (1)(a), a person is deemed to be independent of any registered political party if in the two years prior to his or her nomination for a peerage that person—
   1. was not a member of a registered political party,
   2. had not given public support, by way of public speaking or appearance, to a registered political party, and
   3. had not made a financial donation to a registered political party.
3. In order to achieve the principle specified in subsection (1)(c), the Prime Minister must, other than under the conditions specified in subsection (4), have regard to recommendations made by the Commission for reducing the size of the membership of the House.
4. Following the appointment by the Crown of a Prime Minister at the start of a Parliament, where that person replaces one who led a political party other than the one headed by the Prime Minister, the Prime Minister may recommend peerages on one occasion only without reference to the principle specified in subsection (1)(c), but the number must be no more than 40.

## 4 Commission membership
1. The Commission must consist of nine members.
2. The members of the Commission, including the Chair, must be nominated jointly by the Speaker of the House of Commons and the Lord Speaker.
3. In making nominations under subsection (2), the Speaker and the Lord Speaker may consult such other persons or bodies as they deem appropriate.
4. In making nominations, the Speaker and the Lord Speaker must have regard to the need to ensure that, of members affiliated with registered political parties, the Commission is politically balanced.
5. At least four members of the Commission must be independent of any registered political party, including the Chair of the Commission.
6. At least four members of the Commission must be Privy Counsellors.
7. No-one may be nominated under subsection (5) if at any time in the preceding
two years that person—
   1. has been a member of a registered political party,
   2. has given public support, by way of public speaking or appearance, to a registered political party, or
   3. has made a financial donation to any registered political party.
8. No-one may be nominated who is a Minister of the Crown or holder of a national office in any registered political party.
9. Following nomination by the Speaker and the Lord Speaker under subsection (2), the members may be appointed by the Crown.
10. Except as provided for in subsection (11), a member of the Commission may serve for a non-renewable term of seven years.
11. A member of the Commission may be removed by the Crown on an address by both Houses of Parliament.

## 5 Commission to determine rules and procedure
1. Except as provided for in this Part, the Commission may determine its own rules and procedures.

## 6 Nominations for peerages
1. In addition to considering names placed before it by the Prime Minister under section 2(1), the Commission may propose to the Prime Minister persons that the Commission consider merits the conferment of a peerage.
2. Persons proposed by the Commission under subsection (1) must have no party-political alignment.
3. To qualify for the purposes of subsection (2) a person must not in the past two years—
   1. have been a member of a registered political party,
   2. have given public support, by way of public speaking or appearance, to a registered political party, or
   3. have made a financial donation to a registered political party.
4. For the purpose of proposing names under subsection (1), the Commission may consider persons nominated by members of the public.

## 7 Nominees to meet specific criteria
1. Any person whose name is considered by the Commission under sections 2(1) and 6(1) must comply with the criteria specified in this section.
2. The principal criteria for the recommendation for a peerage are—
   1. conspicuous merit, and
   2. a willingness and capacity to contribute to the work of the House of Lords.
3. The Commission may propose additional criteria as it deems appropriate.
4. In proposing additional criteria, the Commission must have regard to the diversity of the United Kingdom population.
5. Criteria proposed by the Commission under subsection (3) must be laid before both Houses of Parliament and are subject to annulment in pursuance of a resolution of either House of Parliament.
6. Criteria proposed under subsection (3) may not be varied other than on a proposal made by the Commission and subject to the procedure specified in subsection (5).

## 8 Guidelines
1. The Commission may issue guidelines setting out how it will interpret and apply the criteria established under section 7.
2. The guidelines must be publicised by the Commission in such manner as it deems appropriate.
3. The guidelines may be reviewed as the Commission deems appropriate.

## 9 Party leaders to furnish information to the Commission
1. Where names are submitted to the Commission by the Prime Minister acting as a party leader, or on behalf of leaders of other political parties who have been invited to propose people for the conferment of peerages, the leader responsible for each proposal must inform the Commission of the procedure and criteria adopted by the party for the purposes of selecting the name for submission.
2. Leaders of political parties submitting information to the Commission under subsection (1) must supply such other information as may be requested by the Commission.
3. The Prime Minister, where acting as Prime Minister and not as a party leader in making a recommendation for a peerage, must supply such information as may be requested by the Commission.

## 10 Short title, commencement and extent
1. This Act comes into force at the end of the period of three months beginning with the day on which this Act is passed.
2. This Act extends to the United Kingdom.
3. This Act may be cited as the House of Lords (Peerage Nominations) Act 2022.
