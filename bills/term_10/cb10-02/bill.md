# Expanded Wales Asylum Powers Bill 2022
A BILL TO assist refugees in applying for asylum by allowing lawmakers in Wales to process asylum requests.

## 1 Definitions

1. Significant danger refers to potential physical or financial harm due to conditions in the refugee's current environment.

## 2 Processes

1. Refugees judged to be in significant danger will be considered by a committee of lawmakers in Wales. 
2. This committee will be able to bypass existing regulations if it is deemed necessary for the safety or well-being of the refugees.

## 3 Rights

1. Any refugees accepted by the provisions of this act will immediately be considered a permanent resident that has resided in Wales for a period of one year, and shall receive all benefits and rights accorded to such a resident. 
2. The Government will make a commercially reasonable effort to guarantee food and shelter for accepted refugees.
3. Any refugees going through the provisions of this act will be able to appeal any decision made to the existing asylum acceptance apparatus.

## 4 Short title, commencement, and extent

1. This Act may be cited as the Expanded Wales Asylum Powers Act 2022.
2. This Act extends to Wales.
3. This Act comes into force upon royal assent.