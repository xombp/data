# Northern Ireland (Prevention of Deadlock) Bill

*A Bill to make provisions for the holding of early elections, discourage members from preventing the sitting of the Assembly, and for connected purposes.*

## Amendment of Northern Ireland Act 1998

1. The Northern Ireland Act 1998 is amended as follows.
2. In Section 16B, insert new subsection (11) which reads-
> (11) If the Assembly fails to elect a First Minister or deputy First Minister in a period of six months beginning with the day on which the Assembly first meets or upon a vacancy, the Secretary of State shall be responsible for dissolving the Assembly and calling for an early Election.
3. In Section 39, insert new subsection (9) which reads-
> (9) If the Assembly fails to elect a Presiding Officer and deputies in a period of three months beginning with the day on which the Assembly first meets or upon a vacancy, the Secretary of State shall be responsible for dissolving the Assembly and calling for an early Election.
4. In Section 47, insert new subsection (12) which reads-
> (12) If the Assembly does not sit for a prolonged period of more than 6 weeks as a consequence of the Assembly failing to elect a Presiding Officer, First Minister, Deputy First Minister, or their deputies, members of the Assembly shall cease to receive remunerations by means of salaries or allowances until the Assembly is again able to sit.

## 5 Short title, commencement and extent

1. This Act may be cited as the Northern Ireland (Prevention of Deadlock) Act 2022.
2. This Act comes into force one month after royal assent.
3. This Act extends to the United Kingdom.