# National Cycling Infrastructure Bill 2022
A bill to expand Britain’s cycling infrastructure.
Be it enacted by the Queen's most Excellent Majesty, by and with the advice and consent of the
Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the
authority of the same, as follows:—
## 1 Cycleways
1. The Government shall commit to funding cycleways in cities across the United Kingdom.
    1. The following cities shall be designated for the construction of cycleways:
        1. Manchester
		1. Birmingham
		3. Glasgow
		4. Cardiff
		5. Leeds
		6. Liverpool
		7. Southampton
	2. Any cycling infrastructure must have all of the following features to qualify as a cycleway
		1. Cycling lanes are fully separated from automobile traffic
		2. No single-level intersections with automobile traffic
		3. All cycling lanes and paths are clearly marked by both signs and blue
markings painted on the lanes
		4. Cycling lanes are paved with asphalt
2. Each city shall receive £450,000,000 from the Department of Transport to construct their
cycleway network, with the option to request additional funding from the Department for
Transport.
	1. Approval of requests for additional funds is at the discretion of the Secretary of
State for Transport.
3. Cities not included in Section 1.1.a may make a formal request to the Secretary of State for
Transport, outlining their proposal for a local cycleway network and how government
money will be used. The Secretary of State has discretion in approving or denying this
request.
## 2 Public bike hire scheme
1. The Government shall establish a National Cycle Hire Scheme.
	1. The following cities shall be included in this National Cycle Hire Scheme:
		1. Manchester
		2. Birmingham
		3. Glasgow
		4. Cardiff
		5. Leeds
		6. Liverpool
		7. Southampton
	2. Cities not included on this list may make a formal request to the Secretary of State for Transport, outlining their proposal for implementing the guidelines of the National Cycle Hire Scheme. The Secretary of State for Transport has discretion in approving or denying these requests.
2. Bicycle docking stations shall be installed in locations across the areas where the scheme is applied.
	1. Bicycle docking stations shall be installed at least two kilometers apart from each other.
	2. Electric bikes shall be parked at these locations.
3. If a person wants to hire a bicycle at a bicycle docking station, the person must—
	1. Register as a National Cycle Hire Scheme user
	2. Pay a registration fee
	3. Pay an unlimited short journey fee
	4. Unpark the bicycle from the bicycle docking station
4. A person shall register by the creation of an online account specifically for the National
Cycle Hire Scheme.
5. A person is to have commenced a ‘journey’ when they unpark the bicycle from the bicycle docking station.
	1. A journey is to have ended when they park the bicycle in a bicycle docking station.
	2. A ‘short journey’ shall last for 30 minutes between the commencement of the journey and the parking of the bicycle at the bicycle docking station.
6. If a person has commenced a journey, and the time since the person unparked the bicycle from the bicycle docking station has exceeded 30 minutes, then the journey becomes an ‘extended journey’.
    1. If a journey becomes an ‘extended journey’, the person who hired the bicycle from the bicycle docking station shall pay an extended journey fee every 30 minutes after the journey has become an ‘extended journey’.
7. The Secretary of State for Transport may add additional fees, eliminate or temporarily suspend fees, or adjust the cost of fees.
8. The National Cycle Hire Agency shall be established as part of the Department for Transport. The National Cycle Hire Agency shall have authority over the following areas:
    1. The provision of the digital services required to access the scheme
    2. The installation of bicycle docking stations
    3. The purchase, maintenance, and repairing of bicycles
    4. Setting the terms of use for the scheme, whilst ensuring they are upheld.
9. The Secretary of State may delegate provisions of this bill to other officers or create new
officers as required.
## 3 Protection of cycling trails
1. The Government shall create a Commission for the Preservation of Cycling Trails.
    1. The Commission shall be conduct surveys of all long-distance cycling trails (50 km or longer) in the United Kingdom.
    2. The Commission shall investigate the condition and marking of various trails.
    3. Upon completing its work, the Commission will issue its report. The report shall offer recommendations on how much it would cost to restore and properly mark all longdistance cycling trails in the United Kingdom.
    4. The Commission shall be appointed by the Secretary of State for Transport.

## 4 Short title, commencement and extent
1. This Act may be cited as the National Cycling Infrastructure 2022.
2. This Act comes into force upon royal assent.
3. This Act extends to the United Kingdom.