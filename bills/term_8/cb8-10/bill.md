# Decarbonisation Act (Amendment) Bill
A Bill to amend the Decarbonisation Act 2020 and for connected purposes.

## 1 Amendments
1. After Section (8) insert Section (9) and renumber accordingly:
> "# 9 Ban on Personal Combustion Vehicles
> 1. In this section, "combustion vehicles" refers to any vehicle that utilises a hydrocarbon combustion mechanism to generate its driving power.
> 2. The sale of first-hand combustion vehicles will be barred from January 1 2027, excepting:
>       1. Vehicles used for the purpose of transportation of goods for commercial purposes.
> 	    2. Vehicles used by essential services including but not limited to policing, fire management, waste management and street-cleaning.
> 	    3. Vehicles owned by a corporation succesfully granted a Combustion Bypass Order as outlined in Section (9)."
2. After Section (9) insert Section (10) and renumber accordingly:
> "#  Provisions for Combustion Bypass Orders
> 1. A registered not-for-profit charitable organisation may apply for a Combustion Bypass Order.
> 2. The Combustion Bypass Order is to be granted by the Ministry of Transport if the applicant fulfills the following requirements:
>       1. Their use of a non-excepted combustion vehicle is provably used for operations within the organisation.
>       2. They cannot viably switch to an electric alternative.
> 	    3. The use does not contravene any other statute laid out in this Act.
3. The fulfillment of these requirements by the applicant is to be assessed by experts on climate change law and transportation within the Ministry of Transport."

## 2 Short title, commencement and extent
1. This Act may be cited as the Decarbonisation Act (Amendment) Act 2022.
2. This Act enters into force upon receiving royal assent.
3. This Act extends as far as provisions amended.