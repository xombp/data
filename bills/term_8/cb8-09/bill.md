# Employment Rights (Dismissal and Re-engagement) Bill

A Bill to provide safeguards for workers, firms and trade unions against the practice of dismissal and re-engagement on inferior terms and conditions; and for connected purposes.

## 1 Provisions for consultation regarding dismissal and re-engagement
1. The Trade Union and Labour Relations (Consolidation) Act 1992 is amended as follows:—
2. After Part IV Chapter 1, insert:—
> "Chapter 1A: Procedure for handling dismissal and re-engagement
>
> ### 187A Duty of employer to consult representatives
> 1. The provisions of this section are relevant to an employer of an organisation with 50 or more employees, due to an economic difficulty in which there is a real threat to continued employment within the organisation, and in which one or both of the following matters apply
>   1. Decisions may have to be taken to terminate the contracts of 10 or more employees for reasons other than conduct or capbility.
>   2. Measures are anticipated which are likely to lead to substantial chanes in organisations or contractual relations affecting 15 or more employees.
> 2. The employer is to consult with a view of reaching an agreement to avoid decisions being taken to enable provisions as outlined in subsection (1).
> 3. The consultations under subsection (2) are to take place with all persons who are appropriate representatives of the employees who are or may be affected by the provisions of subsection (1).
> 4. The consultations are to take place as soon as is reasonably practicable and in good time for a decision to be reached on the provisions of subsection (1).
> 5. "Appropriate representatives" is defined as under section (188)(1B).

## 2 Provisions for protecting employment contracts against dismissal and re-engagement
1. The Employment Rights Act 1996 is amended as follows:—
2. After Part IIA, insert:—
> "Part IIB: Procedure for the protection of employement contracts
> 
> ### 27C Variation of employment contracts
> 1. Any variation to an employment contract is void if it—
>   1. was obtained under the threat of dismissal, or following dismissal
>   2. is less favourable to the employee than the pre-existing provision
> 2. In subsection (1)(b), the definition of "less favourable" includes, but is not limited to—
>   1. Lower wages
>   2. Reduced out-of-work benefits
>   3. Reduced hours
>       1. Including variation to a "zero hours" contract
>       2. "Zero hours" contract is defined as under Section IIA of the Employee Rights Act 1996
> 3. The definition in any particular case, however, is ultimately to be determined by the perception of a reasonable employee in the position of the employee affected under terms of (IIB)(27C)(1)."

## 3 Formation of a commission to ensure harmonious transformation
1. The extra-Parliamentary "Committee on Fair Employment Practice", hereafter referred to as the Committee, will be formed.
2. The Appointments Commission will have the job of appointing 15 members of this Committee, consisting of—
    1. A fair and substantial number of trades union representatives
    2. A fair and substantial number of industry firm representatives
    3. A fair and substantial number of legal experts on employment relations
3. The Committee will be tasked to engage in consultations outlined in Section (1)(187A) with the goal of reaching a fair and harmonious transformation of employees into jobs similar to their old, and of new contracted employees into jobs appropriate for their sector.

## 4 Short title, commencement and extent
1. This Act may be cited as the Employment Rights (Dismissal and Re-engagement) Act 2022.
2. This Act extends as far as provisions amended under Sections (1) and (2), and to the whole United Kingdom under Section (3).
3. This Act comes into force upon achieving royal assent.