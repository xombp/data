# Wealth Taxation Bill
A bill to enhance the United Kingdom’s revenue stream for purposes of combating underdevelopment

Be it enacted by the Queen's most Excellent Majesty, by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:—


## 1 Definition of Wealth

1. For the purposes of this bill, Wealth is defined as the combined value of an individual's cash, bank deposits, real estate, insurance and pension assets, financial securities and ownership in unincorporated businesses.

## 2 Enactment of the Tax

1. A tax of 1% on all wealth above £500,000 (£1,000,000 in the case of a jointly assessed couple) will be enacted in 2022
Her Majesty’s Treasury will be empowered to collect said tax.

## 3 Special Payment Conditions

1. In the case of constrained liquidity (asset rich, cash poor) for an individual or jointly assessed couple, deferral of payment shall be considered. 
2. A tax made in respect to pension wealth shall be carried out upon the payout of a pension lump sum made upon retirement. 
3. The payment period for a one-off tax may be extended under predefined conditions.
Where neither are sufficient solutions, a statutory time to pay scheme shall allow the individual or couple to pay in installments.

## 4 Short Title, Commencement, and Extent.

1. This Act may be cited as the Wealth Tax Act
2. This Act extends to the entire United Kingdom of Great Britain and Northern Ireland.
3. This Act comes into force upon receiving royal assent.