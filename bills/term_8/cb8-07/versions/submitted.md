Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the
authority of the same, as follows:—

## 1 Charter of employee obligations
1. All businesses or organizations with more than 50 paid employees must establish a charter of
employee obligations.
    1. The charter must include the hours that employees are obligated to work
    2. All new employees, as well as all current employees at the time of the bill’s passage,
must be given a chance to review their employer’s charter.

## 2 Right to disconnect
1. All employees of organizations with 50 or more paid employees will have the right to
disconnect.
    1. ‘Right to disconnect’ refers to an employee’s right to not engage in any work-related
activities or communications outside of working hours
    2. Emergency activities that are essential to the organization’s operations or to national
security will be exempt from this requirement.
2. Employees may voluntarily accept work outside their designated working hours.

## 3 Overtime pay
1. All work done as an extension of an employee’s designated working hours is to be paid at a
minimum of time and a half overtime.
