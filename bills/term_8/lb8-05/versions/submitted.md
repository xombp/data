## 1 Definitions
1. P&O Ferries refers to the corporation 'P&O FERRIES LIMITED' (referred to hereafter as "the Corporation).

## 2 Nationalisation
1. All assets held by the Corporation are to be brought into government ownership.
2. All services are to be continued as normal.
3. All staff will remain on their pre-fire-and-rehire payrolls.

## 3 Compensation
1. Remuneration for the stakeholders of the Corporation will be investigated by the Tripartite Economic Committee and carried out by the Department for Economic Affairs.
2. This Bill recommends a sum not lower than the sum of profits for the year preceeding the passing of this bill.
3. Remuneration for the workers of the Corporation will consist of:
    1. For those fired on 17 March 2022:
        1. The immediate return of their prior positions in the Corporation
        2. Wages of the same muneration as prior to firing.
    2. For those hired on unlawful wages as replacements since 17 March 2022:
        1. Wages consistent with similar positions in the discipline.
        2. Discussions with the Corporation and trade unions on offloading onto other jobs to ensure that both workers fired and new workers receive fair compensation.

## 4 Short title, commencement and extent
1. This Act may be cited as the P&O Ferries (Nationalisation) Act 2022.
2. This Act extends to the entire United Kingdom.
3. This Act comes into force upon achieving royal assent.
