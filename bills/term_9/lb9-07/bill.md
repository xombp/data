# Public Order Bill

*A bill to prevent serious disruption as a consequence of public processions or assemblies, to expand the powers to prevent such disruptions, and for connected purposes*

## 1 Public nuisance and serious disruption

1. Persons participating in a public procession or in an assembly may not cause serious disruption to activities, life of the community, noise, public nuisance or inconvenience.
2. The senior police officer may give directions imposing on the persons organising or taking part in a public procession or assembly to prevent disorder, damage, disruption, impact, or intimidation.
3. The Secretary of State may by regulations make provision about the meaning of-
   1. serious disruption to activities
   2. serious disruption to life of the community, and
   3. serious inconvenience.
4. The Secretary of State may regulations make additional provision as they consider appropriate to prevent serious disruption to activities and life of the community.
5. Regulations under this Section are subject to the negative resolution procedure.

## 2 Intentionally or recklessly causing public nuisance or disruption

1. A person commits an offence if—
   1. the person—
      1.  does an act, or
      2.  omits to do an act that they are required to do by any enactment or rule of law,
   2.  the person’s act or omission—
       1.  causes serious harm to the public or a section of the public, or
       2.  obstructs the public or a section of the public in the exercise or enjoyment of a right that may be exercised or enjoyed by the public at large, and
   3.  the person intends that their act or omission will have a consequence mentioned in paragraph (b) or is reckless as to whether it will have such a consequence.
2.  For the purposes of subsection (1) an act or omission causes serious harm to a person if, as a result, the person—
    1.  suffers death, personal injury or disease,
    2.  suffers loss of, or damage to, property,
    3.  suffers disruption to activities, life of the community, or inconvenience as defined by section 1 of this act.
    4.  suffers serious distress, serious annoyance, serious inconvenience or serious loss of amenity, or
    5.  is put at risk of suffering anything mentioned in paragraphs (a) to (d)
3.  It is a defence for a person charged with an offence under subsection (1) to prove that they had a reasonable excuse for the act or omission mentioned in paragraph (a) of that subsection.
4.  A person guilty of an offence under subsection (1) is liable—
    1.  on summary conviction, to imprisonment for a term not exceeding 12 months, to a fine or to both;
    2.  on conviction on indictment, to imprisonment for a term not exceeding 10 years, to a fine or to both.

## 3 Powers to prevent repeat offenses

1. A judge may place further restrictions upon a person convicted or sentenced for an offence under section 2 of this act to prevent repeat offences.
2. These restrictions may include but are not limited to-
   1. preventing the person from participating in public processions or assemblies,
   2. preventing the person from meeting specific individuals,
   3. preventing the person from accessing certain sites, or
   4. preventing the person from participating in the public discourse.
3. Any restrictions made under this section may not exceed 10 years.
4. The Secretary of State may by regulations authorise further restrictions as they consider appropriate.
5. Regulations under this Section are subject to the negative resolution procedure.

## 4 Extent, commencement, and short title
1. This Act may be cited as the Public Order Bill 2022
2. This Act extends to the United Kingdom of Great Britain and Northern Ireland
3. This act comes into force six months upon royal assent.

