# Fair Employee Representation Bill
A bill to establish worker representation through Inclusive Ownership Funds and employee board seats

Be it enacted by the Queen's most Excellent Majesty, by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:-

## 1 Organization of Inclusive Ownership Funds

1. Any company with more than 250 United Kingdom-based employees that issues new shares after this bill comes into force must establish an Inclusive Ownership Fund (IOF) for their United Kingdom employees.
   1. Whenever a company issues new shares, 12% of these shares must be issued to the company's Inclusive Ownership Fund (IOF).
      1. Each company's fund will be managed by an independent trustee with no connection to the company or its leadership.
   2. Every worker covered by an IOF will be eligible for a company dividend of no more than £600 per year. 
      1. This dividend cap will rise at a rate of 5% each year, until a company distributes at least 75% of its IOF dividend to its employees.
      2. Any dividends over the cap shall be government funds, with half of the amount being earmarked for the Sustainable Transport Fund, and the other half for the Renewable Energy Fund.
   3. IOFs and their trustees will be entitled to any other benefits that the company confers with stock ownership, including but not limited to: Seats on the board of directors, board voting rights, and representation when making major company decisions.
      1. IOF trustees will be assumed to be acting on behalf of workers and their interests. If workers feel that trustees are not acting in their interest, they may lodge a complaint with the Department for Work and Pensions, which could appoint a new trustee if the complaint is found to be accurate. 

## 2 Employee Board Seats

1. Each full year following the enactment of this bill, United Kingdom-based corporations with more than 250 United Kingdom-based employees must grant board seats equivalent to 10% of the board of directors to employee representatives, who shall be freely elected by all employees and contractors of the company.
   1. This minimum board seat requirement shall rise by 5% each year, until being capped at 50%.
   2. All employee board members shall have all the same privileges granted to other board members, including but not limited to: Board voting rights, ability to attend and speak at company board meetings, appropriate compensation, and any bonuses or pay increases that are also given to every other company board member. 

## 3 Exemptions

1. Companies may apply for exemptions from this legislation if they can prove that their essential functions would be hampered by the terms of this legislation.
2. Upon receiving a request for exemption, the Secretary of State for Work and Pensions may decide to reject the exemption, grant a full or partial exemption, or send a government adviser to help the company make changes that will allow it to carry out the provisions of this legislation.

## 4 Enforcement

1. The Department for Work and Pensions shall receive and investigate claims of IOF mismanagement, improper payment of dividends, and other malfeasance related to this legislation.
2. If the Department finds that a company acted improperly, the company shall be fined the equivalent of 110% of the value of the misappropriated funds.
   1. Companies and employees shall both be allowed to appeal for a reconsideration of the decision or fine to the Department for Work and Pensions.
   2. If the value of the improper behavior cannot be determined, then fines shall be at the discretion of the Department for Work and Pensions.
3. The Department for Work and Pensions may issue further regulations and statutory instruments as necessary.

## 5 Short title, commencement and extent

1. This Act may be cited as the Fair Employee Representation Act 2022.
2. This Act comes into force four months after royal assent.
3. This Act extends to the United Kingdom.
