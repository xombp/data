# Sustainability and Heritage Fund Bill
A Bill to establish a sovereign wealth fund derived from the United Kingdom's resource sector to invest in the nation's future wealth and wellbeing.

BE IT ENACTED by the Queen's most Excellent Majesty, by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:

## 1 The British Sustainability and Heritage Fund
1. The government of the United Kingdom will establish a sovereign wealth fund.
2. This sovereign wealth fund will be referred to as the British Sustainability and Heritage Fund (BSHF).
3. The activities of the BSHF will be managed by a standing committee comprising independent experts and government officials that will be overseen and held accountable by Her Majesty's Treasury.

## 2 Funding for the BSHF
1. The BSHF will collect at least 20% of non-renewable resource (NRR) revenues made within the United Kingdom, including the North Sea offshore drilling sector, with a large .
2. The BSHF will collect at least 5% of monetary transfers made in the financial sector, including the London Stock Exchange.
3. Returns from BSHF investments will contribute towards its investments. 

## 3 Investments of the BSHF
1. The BSHF will provide an annual investment of its collected NRR revenues towards the domestic and global economy.
2. Domestic investments of the BSHF will be primarily targeted at funding pensions and other government transfer payments, as well as protection of the natural environment.
3. The BSHF will guarantee at least 25% of its annual spending will go towards the Scottish economy and government.
4. Foreign investments of the BSHF will go towards owning stocks and shares in overseas private enterprises.
5. The BSHF will not invest in firms in breach of British or international law, as well as firms indicted in a significant pattern of violations of human rights, labour rights, or environmental protections. Any firm found engaging in activities under these criteria will face immediate divestment by the BHSF unless they cease these activities.
6. Recognising the volatility of oil prices, the BSHF will invest its savings in the British economy in the event of a significant downturn in production or prices.

## 4 Short title, commencement, and extent
1. This Act may be cited as the Sustainability and Heritage Fund Act 2019.
2. This Act comes into force on the passing of the Act.
3. This Act extends to the whole of the United Kingdom of Great Britain and Northern Ireland and its Overseas Territories.
