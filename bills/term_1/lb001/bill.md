# Northern Ireland Referendum Bill
A Bill to Authorise the Holding of a Referendum in Northern Ireland in Accordance to the Good Friday Agreement

BE IT ENACTED by the Queen's most Excellent Majesty, by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:

## 1 Organisation of the Referendum
1. In parallel with a Referendum on the Exiting the European Union, a referendum shall be held in Northern Ireland.
2. Each British citizen, provided they are eligible to vote in a general election, shall be eligible to vote in the referendum.
3. Each ballot shall contain the following question:
	1. "Do you wish for the Status of Northern Ireland to Change?"
		1. Yes
		2. No

## 2 Protocol in the event of a "Yes" Victory
1. The Government of the United Kingdom shall engage in negotiations to determine the future status of Northern Ireland.
2. The Government of the United Kingdom will commit to changing the present Status of Northern Ireland.
3. Negotiations will be entered into with the Republic of Ireland.

## 3 Short title, commencement and extent
1. This Act may be cited as the Northern Ireland Referendum Act 2019.
2. This Act comes into force on the passing of this Act.
3. This Act extends to Northern Ireland.