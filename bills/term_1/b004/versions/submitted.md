## 1 Vaccinations
1. Vaccine is defined as a Biological preparation that provides active acquired immunity to a disease.
2. All People born after the passing of this act are required to get vaccines, subject to 
3. A person is not required to get a vaccine if:
    1. They have an Immunodeficiency condition which would put their life in danger by receiving vaccines.
    2. They are allergic to compounds in the vaccines that would put their life in danger by receiving vaccines.
    3. They have had a previous serious allergic reaction related to a vaccine.

## 2 The Goals of Offhelc
1. An independent regulatory body will be created. This body will be called “The Office of Public Healthcare” shortened to “OffHelc”
2. Offhelc will oversee vaccination in order to ensure a 90% Vaccination target in order to ensure herd immunity
3. Offhelc will set and monitor NHS maximum waiting times to ensure a good quality
4. Offhel will be in charge of regulating and ensuring a minimum quality of Healthcare
5. Any privatisation of the NHS will have to be approved by a Offhelc, and, and Offhelc will be in charge of issuing contracts

## 3 Powers of Offhelc
1. In order to ensure enforcement of Offhelc’s set quotas, Offhelc will have the power to fine private companies that do not meet the standards.
2. Offhelc will be able to issue quarantines as subject to Part 4

## 4 Quarantine of Areas affected by Contagious Disease
1. Offhelc, in association with Farm Animal Welfare Council and the Department for Environment, Food and Rural affairs, will be able to Quarantine areas subject to this Part.
2. If areas of farmland are found to contain contagious diseases,  Offhelc has the power to:
    1. Cordon off the Farmland to prevent Trespass in an attempt to halt the spread
    2. Seize and Destroy in a safe manner any affected Crop, Livestock, or other produce.
    3. To issue a complete ban on the sale of Crops, Meat or other affected Produce that was produced in affected regions
    4. To Order Vaccination of Animals in regions affected by an Outbreak to try and slow spread

## 5 Short title, commencement and extent
1. This Act may be cited as the Disease Prevention Act
2. This Act comes into force on the passing of this Act.
3. This Act extends to England, Wales, Scotland, and Northern Ireland.
