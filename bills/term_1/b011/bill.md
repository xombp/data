# Tax Relief Bill
A bill to provide economic growth and enhance personal prosperity through tax relief. 

BE IT ENACTED by the Queen's most Excellent Majesty, by and with the advice and consent of the Lords Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:—

## 1 Lowering Tax Thresholds
1. The standard income tax personal allowance shall be increased from £12,500 to £13,500.
2. The 40 percent income tax threshold shall be raised from £50,000 to £55,000.
3. The standard tax-free threshold for inheritance shall be increased from  £325,000 to £500,000.

## 2 Extent, Commencement and Short Title
1. This bill extends to the whole of the United Kingdom of Great Britain and Northern Ireland.
2. This bill shall be referred to as the "Tax Relief Act".
3. This bill will come into effect upon receiving royal assent.
 