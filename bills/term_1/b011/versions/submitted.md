## 1 Lowering Tax Thresholds
1. The standard income tax personal allowance shall be increased from £12,500 to £13,500.
2. The 40 percent income tax threshold shall be raised from £50,000 to £55,000.
3. The standard tax-free threshold for inheritance shall be increased from  £325,000 to £500,000.

## 2 Extent, Commencement and Short Title
1. This bill extends to the whole of the United Kingdom of Great Britain and Northern Ireland.
2. This bill shall be referred to as the "Tax Relief Act".
3. This bill will come into effect upon receiving royal assent.
