# Armenian Genocide (Recognition) Bill
A bill to require Her Majesty's Government to formally recognise the Armenian genocide of 1915-16.

Be it enacted by the King's most Excellent Majesty, by and with the advice
and consent of the Lords Spiritual and Temporal, and Commons, in this present
Parliament assembled, and by the authority of the same, as follows:—
## 1 Recognition of Armenian genocide
1. Her Majesty’s Government must formally recognise that the killings of Armenians in the Ottoman Empire and the surrounding regions in 1915-16 were genocide.
2. The Secretary of State must, as soon as reasonably practicable, make a written statement to the House of Commons—
    1. stating that Her Majesty’s Government formally recognises the Armenian genocide, and
    2. etting out what steps Her Majesty’s Government intends to take in accordance with that recognition.
3. In this Act, "genocide" has the meaning given in Article II of the Convention on the Prevention and Punishment of the Crime of Genocide and Article 6 of the Rome Statute of the International Criminal Court.

## 2 Extent, commencement and short title
1. This Act extends to the whole of the United Kingdom.
2. This Act comes into force on the day on which it is passed.
3. This Act may be cited as the Armenian Genocide (Recognition) Act 2023.