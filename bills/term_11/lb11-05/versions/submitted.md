## 1 Amendment of the Working Time Regulations 1998
1. The Working Time Regulations 1998 are amended as follows.
2. In Regulation 4(1), for “48” substitute “32”.
3. In Regulation 5A(1)(b), for “40” substitute “32”.
4. After Regulation 4, insert—
> ### Remuneration of working time exceeding the maximum
> 4A Where a worker has, pursuant to regulation 5 (Agreement to exclude the maximum), given their agreement in writing to working time exceeding the maximum specified in regulation 4, such work shall be remunerated at a rate not less than one and one-half times the worker’s ordinary rate of pay.

## 2 Extent, commencement and short title
1. This Act extends to England and Wales and Scotland.
2. This Act comes into force at the end of the period of two months beginning with the day on which it is passed.
3. This Act may be cited as the Working Time Regulations (Amendment) Act
2023.
