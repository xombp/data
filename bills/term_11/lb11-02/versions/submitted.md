## 1 Definitions
1. For the purpose of this Act:
    1. "Healthcare services" means any services relating to the prevention, diagnosis, treatment or management of illness, injury or disability, and includes mental health services.
    2. "Healthcare provider" means any person or organization that provides healthcare services, including the National Health Service (NHS) and private healthcare providers.
    3. "Eligible individual" means any UK citizen or resident who is entitled to access healthcare services.

## 2 Duty to Provide Healthcare Services
1. Every eligible individual shall be entitled to receive healthcare services, as determined by medical need, without discrimination on the basis of race, gender, age, sexual orientation, religion or any other protected characteristic.
2. Healthcare providers shall have a duty to provide healthcare services in accordance with this Act.
3. The NHS shall continue to provide free healthcare services to eligible individuals, in accordance with the NHS Constitution.
4. No healthcare provider may discriminate against an individual seeking reproductive healthcare services, including but not limited to contraception, fertility treatments, and abortion.
5. No healthcare provider may discriminate against an individual seeking gender-affirming care, including but not limited to hormone therapy, gender confirmation surgery, and mental health services related to gender identity.

## 3 Provision of Information
1. Healthcare providers shall be required to provide information to eligible individuals about the healthcare services available to them, including information about the services provided by the NHS.
2. The NHS shall provide information to eligible individuals about their rights and entitlements under this Act.

## 4 Patient Rights and Mental Health Services
1. Every patient has the right to access mental health services, including counseling and therapy, without discrimination or prejudice.
2. Mental health services must be provided in a timely and accessible manner, taking into account the specific needs and preferences of the patient.
3. Patients have the right to confidentiality and privacy in their mental health treatment, including the right to control who has access to their mental health records.
4. Mental health professionals must receive appropriate training in order to provide culturally competent and sensitive care to patients from diverse backgrounds, including but not limited to LGBTQ+ patients, patients with disabilities, and patients from ethnic and racial minority groups.
5. Patients have the right to participate in their own mental health treatment decisions, and must be provided with sufficient information to make informed choices about their care.
6. Patients have the right to file complaints or appeals if they feel that their mental health rights have been violated or that they have not received appropriate care.
7. The government will allocate sufficient funds to ensure that mental health services are adequately staffed, resourced, and equipped to meet the needs of patients. This includes funding for research, training, and education to improve mental health care for all.

## 5 Monitoring and Enforcement
1. The Secretary of State shall be responsible for monitoring compliance with this Act.
2. The Secretary of State shall have the power to investigate any breach of this Act.
3. The Secretary of State shall have the power to take enforcement action against any healthcare provider that breaches this Act, including the power to impose financial penalties.
4. Any individual who believes that their rights under this Act have been breached shall have the right to make a complaint to the Secretary of State.

## 6 Short title, commencement and extent
1. This Act may be cited as the Healthcare Access Act 2023.
2. This Act shall come into force on 1st January 2024.
3. This Act extends to England, Wales, Scotland and Northern Ireland.
