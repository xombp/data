## 1 Permitting young persons to carry out voluntary work on a heritage railway or tramway

1. Nothing in section 1(1) of the Employment of Women, Young Persons and Children Act 1920, or in any other enactment relating to the prohibition or regulation of employment, shall be taken as preventing young persons from undertaking voluntary work on a heritage railway or a heritage tramway.

## 2 Interpretation
1. For the purposes of this Act—
    1. “heritage railway” and “heritage tramway” have the same respective meanings as in regulation 2 of the Health and Safety (Enforcing Authority for Railways and Other Guided Transport Systems) Regulations 2006 (S.I. 2006/557) whether or not any such body is carried on for profit;
    2. “voluntary work” means an activity carried out unpaid (except for any travel or other out-of-pocket expenses) by a young person on a heritage railway or a heritage tramway with the aim of—
        1. providing benefit to that body directly; or
        2. providing benefit to the young person concerned regardless of whether incidentally any benefit may also be conferred on that body; and
        3. “young person” has the same meaning as “child” in section 558 of the Education Act 1996 save that the person concerned must have attained the age of 12 years for the purposes of paragraph (b)(i) above or the age of 10 years for the purposes of paragraph (b)(ii) above.

## 3 Extent, commencement and short title
1. This Act extends to England and Wales, Scotland and Northern Ireland.
2. This Act comes into force on the day on which it is passed.
3. This Act may be cited as the Heritage Railways and Tramways (Voluntary Work) Act 2023.
