Whereas it is in accord with the established constitutional position that no law hereafter made by the Parliament of the United Kingdom shall infringe upon the right of Devolved Nations to pass laws as part of the law of that Devolved Nation otherwise than at the request and with the consent of that Devolved Nation:

And whereas it is necessary for the ratifying, confirming and establishing of certain of the said declarations and resolutions that a law be made and enacted in due form by authority of the Parliament of the United Kingdom:

Now, therefore, be it enacted by the King's most Excellent Majesty by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows :— 