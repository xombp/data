## 1 Meaning of "Devolved Nation" in this Act

1. In this Act the expression "Devolved Nation" means any of the following Devolved Nations, that is to say, Northern Ireland, Scotland, and Wales.

## 2 Parliament of the United Kingdom not to infringe on the right of a Devolved Nation to pass laws expect by concent
1. No law and no provision of any law made after the commencement of this Act of Parliament of the United Kingdom shall infringe upon the right of Devolved Nations to pass laws as part of the law of that Devolved Nation which falls within the legislative competence of that Devolved Nation, unless it is expressly declared in that Act that that Devolved Nation has requested, and consented to, the enactment thereof.
2. That is to say, no law and no provision made after the commencement of this Act of Parliament of the United Kingdom shall place additional restrictions on or reduce the extent of the legislative competence of any Devolved Nation, unless it is expressly declared in that Act that that Devolved Nation has requested, and consented to, the enactment thereof.

## 3 Validity of laws made by Parliament of a Devolved Nation

1. No law and no provision of any law made after the commencement of this Act by the Parliament of a Devolved Nation shall be void or inoperative on the ground that it is repugnant to the law of England or the United Kingdom, or to the provisions of any existing or future Act of Parliament of the United Kingdom, or to any order, rule or regulation made under any such Act, and the powers of the Parliament of a Devolved Nations shall include the power to repeal or amend any such Act, order, rule or regulation in so far it falls within the legislative powers of that Devolved Nation.

## 4 Power of Parliament of Devolved Nation to legislate extra-territorially

1. It is hereby declared and enacted that the Parliament of a Devolved Nation has full power to make laws having extra-territorial operation.

## 5 Parliament of United Kingdom not to legislate for Devolved Nation except by consent

1. No Act of Parliament of the United Kingdom which falls within the legislative competence of a Devolved Nation passed after the commencement of this Act shall extend, or be deemed to extend, to a Devolved Nation as part of the law of that Devolved Nation, unless it is expressly declared in that Act that that Devolved Nation has requested, and consented to, the enactment thereof.

## 6 Saving with respect to Northern Ireland
1. The Parliament of the United Kingdom may pass certain laws on behalf of Northern Ireland in the event that the Northern Ireland Assembly fails to meet over an extended period time. 
2. These laws include, but are not limited to-
    1. laws ensuring the continued functioning of the state and vital services,
    2. pass laws to ensure the protection of human rights and the well-being of the citizens of Northern Ireland, and
    3. to honour any international obligations the United Kingdom or Northern Ireland might have in regards to Northern Ireland

## 7 Short title

1. This Act may be cited as the Constitution Act 2023
2. This Act comes into force on the day on which the Act is passed.
3. This Act extends to the United Kingdom.
