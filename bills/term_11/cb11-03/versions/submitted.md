## 1 Parliamentary and Diplomatic Protection

1. The Parliamentary and Diplomatic Protection branch of the London Metropolitan Police Service shall be divided into two components:
    1. The Diplomatic Protection Unit, which shall remain a part of the Metropolitan Police Service and will be tasked with protecting all recognized diplomatic missions within the United Kingdom.
    2. The Parliamentary Security Agency, which shall be organized as an independent agency under the jurisdiction of the Secretary of State for the Home Department, and which shall be tasked with protecting MPs and government ministers both within and outside Greater London.
        1. The Parliamentary Security Agency shall be led by the Director of the Parliamentary Security Agency, who shall be appointed by the Secretary of State for the Home Department. 
        2. The Director of the Parliamentary Security Agency shall be allowed to create additional leadership offices and positions as needed to further the agency's mission.
        3. The Parliamentary Security Agency will absorb the Parliamentary Security Department and take over its function of protecting the chambers of Parliament.

## 2 Security Procedure

1. All MPs shall be assigned a minimum of two plain-clothes police or Parliamentary Security Agency officers as protection whenever the MP is present at a public event outside the Palace of Westminster.
    1. All Cabinet ministers shall be assigned a minimum of three plain-clothes police or Parliamentary Security Agency officers as protection whenever the minister is present at a public event outside the Palace of Westminster.
    2. The Prime Minister shall be assigned a minimum of four plain-clothes officers and one uniformed officer of the Parliamentary Security Agency as protection whenever the Prime Minister is present at a public event outside the Palace of Westminster.
2. All police or Parliamentary Security Agency officers assigned to protect an MP, Cabinet member, or Prime Minister must carry a taser while on duty, as well as receive specialized training on how to use their taser in the event of a security incident.
3. A joint commission of the Parliamentary Security Agency and the Metropolitan Police Force will be formed, with the goal of studying security procedures in the Palace of Westminster and how to best secure the Palace of Westminster against attacks and security incidents.
    1. Upon completion of this study, the commission must issue a report and present its findings to the House of Commons.
4. HM's Treasury shall conduct a study of the aforementioned items to determine the minimum amount of funding necessary to implement these changes. Upon completion of the study, HM's Treasury shall be charged with budgeting the appropriate amount of funding to fully enact this legislation.
## 3 Short title, commencement and extent

1. This Act may be cited as the Parliamentary Security Act 2023.
2. This Act comes into force six months after Royal Assent.
3. This Act extends to the United Kingdom.
