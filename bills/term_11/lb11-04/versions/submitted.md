Be it enacted by the King’s most Excellent Majesty, by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:—

<style>
li td {
  border: 1px solid;
}
li table {
  border-collapse: collapse;
}

</style>

## 1 Extension of period for making Ministerial appointments by six weeks
1. The Northern Ireland Act 1998 has effect as if, during the current post-election period, for subsections (3A) to (3C) of section 16A (appointment of Ministers following an Assembly election) there were substituted—
> “(3A) In this section “the period for filling Ministerial offices” means the period beginning with 13 May 2022 and ending with 8 December 2022.”
2. In this section, “the current post-election period” means the period beginning with 5 May 2022 and ending with the day on which a poll for the election of an Assembly is next held.

## 2 Power to extend period for making Ministerial appointments by a further six weeks
1. The Secretary of State may by regulations made by statutory instrument amend section 1 so as to replace “8 December 2022” with “19 January 2023”.
2. The power in subsection (1)—
    1. may be exercised before, on or after 8 December 2022, but
    2. may not be exercised after the end of the period of seven days beginning with the day on which this Act is passed.
3. A statutory instrument containing regulations under subsection (1) must be laid before Parliament after being made.

## 3 Exercise of departmental functions

1. The absence of Northern Ireland Ministers does not prevent a senior officer of a Northern Ireland department from exercising a function of the department during the current period in which there is no Executive if the officer is satisfied that it is in the public interest to exercise the function during that period.
2. In this section “the current period in which there is no Executive” means the period—
    1. beginning with the day on which this Act was passed, and
    2. ending when an Executive is next formed.
3. The fact that a matter connected with the exercise of a function by a Northern Ireland department has not been discussed and agreed by the Executive Committee of the Northern Ireland Assembly is not to be treated as preventing the exercise of that function as mentioned in subsection (1).
4. The Secretary of State must publish guidance about the exercise of functions by a senior officer of a Northern Ireland department in reliance on this section, including guidance as to the principles to be taken into account in deciding whether or not to exercise a function.
5. Senior officers of Northern Ireland departments must have regard to that guidance.
6. Before publishing guidance under subsection (4) the Secretary of State must have regard to any representations made by members of the Northern 

## 4 Exercise of departmental functions before this Act is passed
1. The absence of Northern Ireland Ministers is not to be treated as having prevented any senior officer of a Northern Ireland department from exercising functions of the department during the period beginning with 28 October 2022 and ending when this Act is passed.
1. Subsection (1) does not apply in relation to the exercise of a function if—
    1. proceedings begun, but not finally decided, before this Act is passed involve a challenge to the validity of that exercise of the function, and
    2. the application of that subsection would affect the outcome of the proceedings,
    but nothing in this subsection prevents the re-exercise of the function in the same way in reliance on section 3(1).

## 5 Exercise of departmental functions: supplementary

1. Sections 3 and 4 have effect despite anything in the Northern Ireland Act 1998, the Departments (Northern Ireland) Order 1999 (S.I. 1999/283 (N.I. 1)) or any other enactment or rule of law that would prevent a senior officer of a Northern Ireland department from exercising departmental functions in the absence of Northern Ireland Ministers.
2. No inference is to be drawn from sections 3 and 4 or this section as to whether or not a senior officer of a Northern Ireland department would otherwise have been prevented from exercising departmental functions.

## 5A Advice and information on options for raising public revenue
1. The Secretary of State may use the powers conferred by this section only—
    1. during the current period in which there is no Executive, and
    2. for the purpose of developing options for raising more public revenue in Northern Ireland or otherwise improving the sustainability of public finances in Northern Ireland.
2. The Secretary of State may direct a Northern Ireland department to—
    1. give the Secretary of State advice or information about such matters as may be specified in the direction;
    2. carry out such consultation as may be specified in the direction.
3. A direction to provide advice or information may include provision about—
    1. the manner or form in which it is to be provided;
    2. when it is to be provided.
4. A direction to carry out a consultation may include provision about—
    1. who is to be consulted;
    2. how the consultation is to be carried out;
    3. the content of the consultation (including provision requiring the department to obtain the approval of the Secretary of State to the content of the consultation before the consultation begins);
    4. the consultation timetable.
5. Where a direction requires a department to give information that is not within its possession, or is not under its control, the department must take reasonable steps to obtain the information for the purpose of complying with the direction.
6. The power under subsection (2)(a) —
    1. may be exercised so as to require two or more departments to give joint advice;
    2. may be exercised so as to require two or more departments jointly to collate information and for one or other of them to give it to the Secretary of State.
7. A direction under this section lapses at the end of the current period in which there is no Executive.
8. In this section "current period in which there is no Executive" means the period—
    1. beginning when the Northern Ireland (Interim Arrangements) Act 2023 is passed, and
    2. ending when an Executive is next formed.
## 5B Advice and information: data protection
1. A direction under section 5A(2)(a) does not require a disclosure of information if the disclosure would contravene the data protection legislation (but in determining whether a disclosure would do so, take into account the duty to comply with a direction under that section).
2. In this section "data protection legislation" has the same meaning as in the Data Protection Act 2018 (see section 3(9) of that Act).


## 6 NI ministerial appointment functions
1. During the current period in which there is no Executive, an appointment function of a Northern Ireland Minister in relation to a specified office may be exercised by the relevant Minister of the Crown.
2. The table defines terms for the purposes of this section.
    <table>
        <tr><td>"specified office"</td><td>"relevant Minister of the Crown"</td></tr>
        <tr><td>Member of the Northern Ireland Judicial Appointments Commission</td><td>Lord Chancellor</td></tr>
        <tr><td>Commissioner for Children and Young People for Northern Ireland</td><td>Secretary of State</td></tr>
    </table>
3. The Secretary of State may by regulations made by statutory instrument add entries to the table.
4. A statutory instrument containing regulations under subsection (3) may not be made unless—
    1. a draft of the instrument has been laid before and approved by a resolution of each House of Parliament, or
    2. the regulations state that the Secretary of State considers it to be expedient for the regulations to be made more quickly than the procedure in paragraph (a) would allow.
5. Where regulations contain a statement under subsection (4)(b)—
    1. the instrument containing the regulations must be laid before Parliament after being made, and
    2. the regulations cease to have effect at the end of the period of 28 days beginning with the day on which the instrument is made unless, during that period, it is approved by a resolution of each House of Parliament.
6. If regulations cease to have effect as a result of subsection (5)(b), that does not—
    1. affect the validity of anything previously done under the regulations, or
    2. prevent the making of new regulations.
7. In calculating the period of 28 days mentioned in subsection (5)(b), no account is to be taken of any whole days that fall within a period during which—
    1. Parliament is dissolved or prorogued, or
    2. both Houses of Parliament are adjourned for more than four days.
8. Before exercising an appointment function in reliance on subsection (1) the relevant Minister of the Crown must consult a Northern Ireland department.
9. Any enactment or document is to have effect, so far as may be necessary for or in consequence of the exercise of any functions by the relevant Minister of the Crown in reliance on this section, as if references to a Northern Ireland Minister included, or were, references to the relevant 

## 7 Minister of the Crown appointment functions

1. Any requirement for a Minister of the Crown to consult, or obtain the approval of, a Northern Ireland Minister or the Executive Committee of the Northern Ireland Assembly before exercising an appointment function has effect, during the current period in which there is no Executive, as a requirement to consult a Northern Ireland department.

## 8 Joint UK appointment functions etc
1. During the current period in which there is no Executive, the Secretary of State may exercise any appointment function of a Northern Ireland Minister that is exercisable jointly with one or more other persons who include the Secretary of State.
2. Before exercising an appointment function in reliance on subsection (1) the Secretary of State must consult a Northern Ireland department.
3. Any enactment or document is to have effect, so far as may be necessary for or in consequence of the exercise of any functions by the Secretary of State in reliance on this section, as if references to a Northern Ireland Minister included, or were, references to the Secretary of State.

## 9 Sections 6 to 8: core definitions

1. In sections 6 to 8—
    1. “appointment function” means—
        1.  the function of appointing a person to an office or recommending a person for appointment;
        2. the function of requesting nominations for an appointment;
        3. the function of determining terms of appointment;
        4. the function of determining remuneration, pensions or other payments in respect of appointments, loss of office or suspension from office;
        5. the function of suspending or removing a person from office, receiving notice of a person’s resignation from office or calling on a person to resign or retire;
        6. the function of approving or being consulted about the exercise of any of the functions listed in paragraphs (a) to (e);
        7. the function of requiring or requesting another person to exercise any of the functions listed in paragraphs (a) to (e);
        8. a function ancillary to any of the functions listed above;
    1. “current period in which there is no Executive” means the period beginning when this section and sections 6 to 8 come into force and ending when an Executive is next formed. 
2. reference in those sections to the function of a person includes a function that is exercisable by that person jointly with one or more other persons.
3. The Secretary of State may by regulations made by statutory instrument amend the definition of “appointment function”.
4. A statutory instrument containing regulations under subsection (3) may not be made unless a draft of the instrument has been laid before and approved by a resolution of each House of Parliament.

## 10 Power to determine salaries and other benefits for Members of the Assembly
1. During a period in which the Northern Ireland Assembly is not functioning, the Secretary of State may make a determination as to—
    1. the salaries or allowances payable under section 47 of the Northern Ireland Act 1998 in respect of some or all of that period, and
    2. allowances or gratuities payable under section 48 of that Act to or in respect of a person ceasing to be a member, or ceasing to hold office, during that period.
2. A determination under subsection (1) must be in writing.
3. As soon as possible after a determination under subsection (1) is made—
    1. the Secretary of State must send it to the Northern Ireland Assembly Commission, and
    2. the Commission must publish it.
4. Section 12 of the Assembly Members (Independent Financial Review and Standards) Act (Northern Ireland) 2011 applies to a determination under subsection (1)(a) of this section as it applies to a determination under section 2(1)(a) of that Act.
5. For the purposes of any determination made by, or by virtue of provision made by, the Assembly under section 48 of the Northern Ireland Act 1998 so far as relating to pensions, members are to be treated as having whatever salary they would have had were it not for any determination made under subsection (1)(a) of this section.
6. A determination under subsection (1) may amend a determination made by, or by virtue of provision made by, the Northern Ireland Assembly under section 47 or 48 of the Northern Ireland Act 1998.
7. A determination made by, or by virtue of provision made by, the Northern Ireland Assembly under section 47 or 48 of the Northern Ireland Act 1998 may not change the effect of a determination made under subsection (1).
8. In this section “period in which the Northern Ireland Assembly is not functioning” means—
    1. the period beginning when this Act is passed and ending with the next day on which the Presiding Officer and deputies are in post, or
    1. any later period—
        1. beginning with the first day after the end of the period in which an Assembly must meet if, at the end of that period, the Presiding Officer and deputies are not in post, and
        2. ending with the next day on which the Presiding Officer and deputies are in post.
9. In subsection (8)—
    1. a reference to a Presiding Officer or deputy being in post is a reference to their being in post having been elected under section 39(1) of the Northern Ireland Act 1998 after—
        1. in the case of the reference in paragraph (a), this Act is passed, or
        2. in the case of a reference in paragraph (b), the day of the poll at which the Assembly referred to in paragraph (b)(i) is elected;
    2. a reference to the period in which an Assembly must meet is a reference to the period referred to in section 31(4) of the Northern Ireland Act 1998.
10. The Northern Ireland Assembly Members (Pay) Act 2018 is repealed.

## 11 Power to set the regional rate for 2023/24
1. The Secretary of State may by regulations made by statutory instrument set the regional rate for the year ending 31 March 2024.
2. The power in subsection (1) may only be used during the current period in which there is no Executive.
3. Regulations under subsection (1) must specify the amount in the pound at which the regional rate is to be levied.
4. Articles 6(3) to (6) and 7(4) and (5) of the Rates Order apply in relation to the setting of a regional rate by regulations under subsection (1) as they apply in relation to the setting of the rate by order under Article 7(1) of the Rates Order (reading references to the Department of Finance as references to the Secretary of State).
5. A reference in the Rates Order to the regional rate (except in a provision applied by subsection (4)) is to be read as including a reference to the regional rate set by the Secretary of State by regulations under subsection (1).
6. A rate set under subsection (1) may be varied, after the end of the current period in which there is no Executive, by an order made by the Department of Finance under Article 7(1) of the Rates Order.
7. An order made by virtue of subsection (6) may set the rate in respect of the whole of the year for which it is made.
8. A statutory instrument containing regulations under subsection (1) is subject to annulment in pursuance of a resolution of the House of Commons.
9. In section 43(2) of the Interpretation Act (Northern Ireland) 1954, the definition of “regional rate” is to be treated as including a reference to any rate set by the Secretary of State under subsection (1).
10. In this section—
    1. “current period in which there is no Executive” means the period beginning when this Act is passed and ending when an Executive is next formed;
    2. “the Rates Order” means the Rates (Northern Ireland) Order 1977 (S.I. 1977/2157 (N.I. 28)). 

## 12 Exception to Assembly power to call for witnesses and documents

1. In section 44 of the Northern Ireland Act 1998 (power to call witnesses and documents), in subsection (4)—
    1. after "in connection with" insert "—(a)";
    2. at the end insert
    > , or
    > (b) the giving of a direction under section 5A of the Northern Ireland (Interim Arrangements) Bill or the exercise of a function in accordance with such a direction.

## 13 Accounts etc to be laid before House of Commons
1. After section 67 of the Northern Ireland Act 1998 insert—
> ### 67A Accounts etc to be laid before House of Commons if no functioning Assembly
> 1. A person must send to the Secretary of State a copy of any minutes, accounts, reports, or other documents, that, during a period in which the Assembly is not functioning, the person lays before the Assembly under a relevant provision.
> 2. The Secretary of State must lay before the House of Commons anything received under subsection (1).
> 3. In this section "relevant provision" means—
>     1. section 10(4), 11(3)(c), 16(4) or 24(2) of the Government Resources and Accounts Act (Northern Ireland) 2001, or
>     2. Article 8 of the Financial Provisions (Northern Ireland) Order 1993 (S.I. 1993/1252 (N.I. 5)).
> 4. In this section "period in which the Assembly is not functioning" means a period—
>     1. beginning with the first day after the end of the period in which an Assembly must meet if, at the end of that period, the Presiding Officer and deputies are not in post, and
>     2. ending with the next day on which the Presiding Officer and deputies are in post.
> 5. In subsection (4) —
>     1. a reference to a Presiding Officer or deputy being in post is a reference to their being in post having been elected under section 39(1) after the day of the poll at which the Assembly referred to in subsection (4) (a) is elected, and
>     1. a reference to the period in which an Assembly must meet is a reference to the period referred to in section 31(4).
2. Section 67A of the Northern Ireland Act 1998 applies in relation to the period in which the Northern Ireland Assembly is not functioning that began before this Act is passed as if that period had begun when this Act is passed.

## 14 Interpretation
1. In this Act—
    1. “enactment” includes any provision of, or of any instrument made under, Northern Ireland legislation (within the meaning given by section 98 of the Northern Ireland Act 1998);
    2. “Minister of the Crown” has the same meaning as in the Ministers of the Crown Act 1975;
    3. “Northern Ireland Minister” includes the First Minister and the deputy First Minister;
    4. “senior officer of a Northern Ireland department” has the same meaning as in the Departments (Northern Ireland) Order 1999 (see Article 2(3) of that Order). 
2. For the purposes of this Act, an Executive is formed once the offices of First Minister and deputy First Minister and those to be held by the other Northern Ireland Ministers are all filled.

## 15 Extent
1. Section 11 extends to Northern Ireland only.
2. The other provisions of this Act extend to England and Wales, Scotland and Northern Ireland.

## 16 Commencement
1. Sections 6 to 9 come into force on such day as the Secretary of State may by regulations made by statutory instrument appoint.
2. The other provisions of this Act come into force on the day on which this Act is passed. 

## 17 Short title
1. This Act may be cited as the Northern Ireland (Interim Arrangements) Act
2023.
