# Languages of the UK (Amendment) Bill
A Bill to amend the Languages of the UK Act 2020 to promote language learning and resolve certain issues.

## 1 Short title, commencement, and interpretation

1. This Act may be cited as the Languages of the UK (Amendment) Act 2023.
2. This Act comes into force on the day on which it receives Royal Assent.
3. This Act extends to England, Wales, Scotland, and Northern Ireland.
4. In this Act, "the 2020 Act" means the Languages of the UK Act 2020.

## 2 Amendments to the 2020 Act

1. The 2020 Act is amended as follows.
2. In section 1(3), after "British Sign Language", insert "and any other language commonly used in the United Kingdom".
3. After section 3, insert the following sections:
> ## 4 Promotion of Language Learning
> 1. The government shall take reasonable steps to promote the learning of languages in the United Kingdom, including but not limited to:
> 	    1. providing funding for language learning programs in schools, colleges, and universities;
> 	    2. encouraging employers to offer language learning opportunities to their employees;
> 	    3. supporting the development and distribution of language learning materials;
> 	    4. collaborating with relevant stakeholders to promote the benefits of language learning and encourage participation in language learning activities.
> 2. The government shall report annually to Parliament on its progress in promoting language learning under this section.
>
> ## 5 Provisions for Sign Languages
>
> 1. The Secretary of State shall ensure that provision is made for the promotion, protection and use of sign languages in the United Kingdom.
> 2. The Secretary of State shall, within one year of the passing of this Act, publish a strategy outlining measures to ensure that the use of sign languages is promoted, protected and supported in the United Kingdom.
> 3. The strategy under subsection (2) shall include, but not be limited to, the following measures:
> 	    1. Promotion of the use of sign languages in public life, including in education, employment, and access to services and information;
> 	    2. Development of training and educational resources for sign language interpreters and sign language users;
> 	    3. Provision of funding for research into the needs and experiences of sign language users in the United Kingdom;
> 	    4. Consultation with sign language users and organisations representing sign language users to ensure that the strategy takes account of their views and experiences; and
> 	    5. Promotion of the use of sign languages in the public sector, including in the provision of public services. 
> 4. The Secretary of State shall report annually to Parliament on the implementation of the strategy under subsection (2).
> 5. In this section, “sign language” means a visual-spatial language that is used as the primary means of communication by a deaf or hard-of-hearing person, including but not limited to British Sign Language (BSL).
