Be it enacted by the King's most Excellent Majesty, by and with the advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by the authority of the same, as follows:-

## 1 Nuclear energy

1. The government shall allocate £70 billion to construct four new nuclear power plants.
	1. One of these three nuclear power plants shall be the Hinkley Point C nuclear power station.
	2. The location and specifications of the remaining three nuclear power plants shall by determined by the Department for Business, Energy, and Industrial Strategy.
	
## 2 Wind energy

1. The government shall allocate £2.5 billion in funding for wind power in Scotland. 
	1. £1.5 billion will go toward constructing a new wind farm off the coast of Fife.
        1. The Department for Business, Energy, and Industrial Strategy will be responsible for surveying potential sites, in consultation with the Scottish Government and local environmental groups.
	2. £500 million will go toward quicker construction of the Viking Wind Farm.
	3. £500 million will go toward the Inch Cape Offshore Wind Farm.

## 3 Tidal lagoon

1. The Secretary of State for Business, Energy, and Industrial Strategy shall establish the Tidal Power Authority (TPA), an independent agency under the authority of the Secretary of State.
	1. The Tidal Power Authority shall be responsible for assessing potential sites in and around Swansea Bay for a tidal lagoon power plant.
	2. The Authority shall have the authority to select private firms to construct and service the Swansea Bay tidal lagoon power plant.
	1. The Secretary of State must approve any proposed contract agreement.
	3. The Authority shall work in consultation with the Welsh devolved government, relevant local governments, and private industry to plan out the tidal lagoon power plant.
	4. The Authority must conduct a study, in consultation with at least one local environmental issues organization, on the environmental and ecosystem effects of the tidal lagoon power plant.

## 4 Assessment

1. The Department for Business, Energy, and Industrial Strategy will be responsible for monitoring and assessing the above-listed projects.
2. No more than two years after this bill comes into effect, the Department for Business, Energy, and Industrial Strategy must release a report examining the current economic and environmental impacts of these projects, their future impacts, and the current budgetary statuses of the listed energy projects.
3. The Department for Business, Energy, and Industrial Strategy may issue further regulations and statutory instruments as necessary.

## 5 Short title, commencement and extent

1. This Act may be cited as the Sustainable Energy Act 2022.
2. This Act comes into force three months after royal assent.
3. This Act extends to the United Kingdom.
